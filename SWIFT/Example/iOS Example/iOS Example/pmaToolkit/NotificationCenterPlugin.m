////
////  NotificationCenterPlugin.m
////  iOS Example
////
////  Created by Natalie Boardway on 4/19/17.
////  Copyright © 2017 Philadelphia Museum of Art. All rights reserved.
////
//
//
//@import UIKit;
//#import "iOS_Example-Swift.h"
//#import "iOS Example-Bridging-Header.h"
//
//
//
//@implementation NotificationCenterPlugin
//
//+(id) getObjectFromNotificationForKey:(NSNotification *) notification withKey:(NSString *) key {
//   return [pmaToolkit getObjectFromNotificationForKey:notification key:key];
//}
//
//+(void) registerNotification: (id )object functionName:(NSString *)function functionType:(NSString *)type {
//    return [pmaToolkit registerNotification:object function:function type:type];
//}
//
//+(void) postNotification:(NSString *)type withParameters:(NSDictionary *)parameters {
//    return [pmaToolkit postNotification:type parameters:parameters];
//}
//
//
//
//@end
