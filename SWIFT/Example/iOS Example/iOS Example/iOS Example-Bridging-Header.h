//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//
@import Foundation;

@interface NotificationCenterPlugin: NSObject

+(NotificationCenterPlugin *)sharedInstance;
//+(void) registerNotification: (id *)object functionName:(NSString *)function functionType:(NSString *)type;
//+(void) postNotification:(NSString *)type withParameters:(NSDictionary *)parameters;
//+(id *) getObjectFromNotificationForKey:(NSNotification *) notification withKey:(NSString *) key;

@end
