This is the latest code for both the Swift and Objective-C version of the pmaToolkit provided by the PMA.

The Swift code should be fully functional, providing that the latest version of Swift is being used.

Known errors with the objc version:
- Complicated way of storing the necessary information about the beacons
- Some of the beacons are not getting parsed correctly, which returns a location of nil, causing problems in the methods trying to assume their location (it's using dictionaries that map locations to percents, and the key can't be nil)
- The parsing problem is mostly related to the locations that have full words (most non-gallery locations), as the regex only looks at the "_[A-Z]_" (only the first underscore, it was using them to italisize the text so I added the closing one) type beacon names.