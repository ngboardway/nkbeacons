//
//  ViewController.m
//  perhaps
//
//  Created by Natalie Boardway on 4/25/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import "ViewController.h"
#import "CoreLocationPlugin.h"
#import "pmaLocationManager.h"
#import "pmaToolkit.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *tv;

@end
static pmaLocationManager *manager = nil;
static pmaToolkit *toolkit = nil;
@implementation ViewController
@synthesize tv;
- (void)viewDidLoad {
    [super viewDidLoad];
    [pmaLocationManager initialize];
    manager = [[pmaLocationManager alloc] init];
    if(!toolkit) {
        toolkit = [[pmaToolkit alloc]init];
    }
    [toolkit registerNotication:self forFunction:@"locationChanged:" withType:@"locationChanged"];
    [tv setText:@"Beacons"];
    [manager startRangingBeacons:mainBuilding];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)locationChanged:(NSNotification*)notif {
    pmaLocation *location = [toolkit  getObjectFromNotificationForKey:notif forKey:@"currentLocation"];
    NSString *text = [tv text];
    text = @"new location name: ";
    [tv setText:text];
}
@end
