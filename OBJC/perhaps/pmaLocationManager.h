//
//  pmaLocationManager.h
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "pmaLocation.h"

@interface pmaLocationManager :NSObject <CLLocationManagerDelegate>

typedef NS_ENUM(NSInteger, locationSensingType){
    mainBuilding = 0
};

@property NSMutableArray<pmaObject*>* objects;
@property NSMutableArray<pmaLocation*>* locations;
@property NSMutableArray<pmaBeacon*>* beacons;

@property pmaLocation* currentLocation;
@property pmaLocation* previousLocation;

@property CLLocationManager* locationManager;
@property NSMutableArray* beaconsInRange;
@property NSMutableDictionary<pmaLocation*, NSNumber*>* locationsInRange;

@property NSDate* unknownLocationTimestample;
@property locationSensingType sensingType;
@property NSMutableDictionary<NSString*, pmaBeacon*> *beaconMap;
-(void)loadBeacons:(void (^)(void))handler;
-(void)loadLocations:(void (^)(void))handler;
-(enum types)getTypesForLocation:(NSString*) locationName withLocationTitle:(NSString*)title;
-(void)startRangingBeacons:(locationSensingType) sensingType;
-(void)startRangingBeaconsInRegion;
-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region;
-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region;
-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)clbeacons
              inRegion:(CLBeaconRegion*)region;
-(void)startUpdateHeading;
-(void)stopUpdateHeading;
-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading;
-(void)addBeaconToBeaconsInRange:(CLBeacon*)beacon withProximity:(CLProximity)proximity;
-(NSMutableDictionary*)countOccurenceForBeaconsInRange;
-(NSMutableDictionary*)calculateRelativeProbabilityDistributionForBeaconsInRange:(NSMutableDictionary*)listofBeaconsWithOccurenceCount;
-(NSMutableDictionary*)calculateProbabilityForLocationsFromBeaconsInRange: (NSMutableDictionary*)probabilityForEachBeacon;
-(NSMutableDictionary*)assumeCurrentLocation:(NSMutableDictionary*)possibilityForEachLocation;
-(void)processBeaconsForMainBuilding:(NSArray*)clBeacons;
-(void)scanForMainBuildingBeaconsInRange;
-(void)updateCurrentLocation:(pmaLocation*)location;
-(pmaBeacon*)getPMABeaconForCLBeacon:(CLBeacon*)originalBeacon;
-(pmaLocation*)getLocationForBeacon:(pmaBeacon*)beacon;
-(pmaLocation*)getCurrentLocation;
-(BOOL)areBeaconsLoaded;
-(BOOL)areLocationsLoaded;
-(pmaLocation*)getLocationFromBeaconAlias:(NSString*)alias;
-(pmaLocation*)getLocationForName:(NSString*)name;
-(pmaLocation*)getNKLocationName:(NSString*)name;
-(pmaBeacon*)getBeaconFromAlias:(NSString*)alias;
-(void)initializeBeaconsInRangeWithBeacon:(pmaBeacon*)beacon;
@end
