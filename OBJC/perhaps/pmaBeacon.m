//
//  pmaBeacon.m
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "pmaBeacon.h"

@implementation pmaBeacon
@synthesize alias;
@synthesize major;
@synthesize minor;

@synthesize originalbeacon;
@synthesize lastSeen;

-(pmaBeacon *) copyWithZone:(NSZone *)zone {
    pmaBeacon *copy = [[pmaBeacon alloc] initWithBeaconFields:self.alias withMajor:self.major withMinor:self.minor withOriginalBeacon:self.originalbeacon];
    return copy;
}

-(pmaBeacon*) initWithBeaconFields:(NSString *)passedAlias withMajor:(NSInteger)passedMajor withMinor:(NSInteger)passedMinor withOriginalBeacon: (CLBeacon*)beacon{
    self = [super init];
    if (self) {
        
        alias = [passedAlias copy];
        major = passedMajor;
        minor = passedMinor;
        originalbeacon = [beacon copy];
    }
    return self;
}

@end
