//
//  pmaCacheManager.h
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface pmaCacheManager : NSObject
typedef NS_ENUM(NSInteger, JSONType) {
    beacon = 0,
    location = 1,
};

@property JSONType type;
-(id) loadJSONFileForType:(NSInteger*)dataType;

@end
