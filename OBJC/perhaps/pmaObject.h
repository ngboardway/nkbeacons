//
//  pmaObject.h
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface pmaObject: NSObject

@property NSString *title;
@property NSArray *location;
@property NSInteger *objectID;
@property NSString *URL;
@property BOOL isOnDisplay;

@end
