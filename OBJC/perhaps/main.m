//
//  main.m
//  perhaps
//
//  Created by Natalie Boardway on 4/25/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
