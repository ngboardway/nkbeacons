//
//  pmaLocation.h
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "pmaBeacon.h"
#import "pmaObject.h"

@interface pmaLocation: NSObject <NSCopying>

@property NSString *name;
@property BOOL enabled;
@property NSString *title;

typedef NS_ENUM(NSInteger, floors) {
    ground = 0,
    first = 1,
    second = 2
};

typedef NS_ENUM(NSInteger, types) {
    gallery = 0,
    bathroom = 1,
    stairs = 2,
    elevator = 3,
    food = 4,
    store = 5,
    info = 6,
    NK = 7
} ;


@property  floors floor;
@property  types type;

@property NSMutableArray<pmaObject*> *objects;
@property NSMutableArray<pmaBeacon*> *beacons;

-(pmaLocation*) initWithLocationFields:(NSString*)locationName onFloor:(enum floors)locationFloor ofType:(enum types)locationType isEnabled:(BOOL)isEnabled;
-(BOOL)respondsToBeacon:(pmaBeacon*) beacon;
-(BOOL)isObjectAtLocation:(pmaObject*) object;
-(pmaLocation *) copyWithZone:(NSZone *)zone;
@end
