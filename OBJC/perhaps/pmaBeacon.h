//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface pmaBeacon: NSObject <NSCopying>

@property NSString *alias;
@property NSInteger major;
@property NSInteger minor;
@property CLBeacon *originalbeacon;
@property NSDate *lastSeen;

-(pmaBeacon*) copyWithZone:(NSZone*) zone;
-(pmaBeacon*) initWithBeaconFields: (NSString*) alias withMajor: (NSInteger)major withMinor: (NSInteger)minor withOriginalBeacon: (CLBeacon*)beacon;
@end
