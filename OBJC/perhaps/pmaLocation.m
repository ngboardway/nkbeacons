//
//  pmaLocation.m
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "pmaLocation.h"

@implementation pmaLocation

@synthesize name;
@synthesize enabled;
@synthesize floor;
@synthesize type;
@synthesize title;
@synthesize objects;
@synthesize beacons;

-(BOOL)respondsToBeacon:(pmaBeacon *)beacon {
    for (pmaBeacon *b in beacons) {
        if (b.major == beacon.major && b.minor == beacon.minor) {
            return YES;
        }
    }
    return NO;
}

-(BOOL)isObjectAtLocation:(pmaObject *)object {
    for (pmaObject *obj in objects) {
        if(obj.objectID == object.objectID) {
            return YES;
        }
    }
    return NO;
}

-(pmaLocation*) initWithLocationFields:(NSString*)locationName onFloor:(enum floors)locationFloor ofType:(enum types)locationType isEnabled:(BOOL)isEnabled {
    self = [super init];
    if (self) {
        name = locationName;
        floor = locationFloor;
        type = locationType;
        enabled = isEnabled;
        return self;
    }
    return nil;
}
-(pmaLocation *) copyWithZone:(NSZone *)zone {
    pmaLocation *copy = [[pmaLocation alloc] initWithLocationFields:self.name onFloor:self.floor ofType:self.type isEnabled:self.enabled];
    return copy;
}

@end
