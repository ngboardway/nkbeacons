//
//  pmaCacheManager.m
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "pmaCacheManager.h"
#import <UIKit/UIKit.h>

@implementation pmaCacheManager
@synthesize type;

-(id) loadJSONFileForType:(NSInteger *)dataType {
    NSString *pathName = @"";
    if(dataType == 0) {
        pathName = @"hackathon-ibeacon";
    } else {
        pathName = @"hackathon-locations";
    }
    NSLog(@"%@", @"This is the filename");
    NSLog(@"%@", pathName);
    NSBundle *main = [NSBundle mainBundle];
    NSString *fullPath = [main pathForResource:pathName ofType:@"json"];
    NSData *preData = [NSData dataWithContentsOfFile:fullPath];
    id jsonObject = [NSJSONSerialization JSONObjectWithData:preData options: NSJSONReadingAllowFragments error: nil];
    
    if (jsonObject != nil) {
        return jsonObject;
    } else {
        return nil;
    }
    return nil;
}

@end
