//
//  CoreLocationPlugin.m
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import "CoreLocationPlugin.h"
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import<CoreBluetooth/CoreBluetooth.h>
#import "pmaLocationManager.h"

@implementation CoreLocationPlugin

+(CLBeaconRegion*) createRegion:(NSUUID *)UUID withIdentifier:(NSString *)identifier {
    return [[CLBeaconRegion alloc] initWithProximityUUID:UUID identifier:identifier];
}

+(NSUUID *)createNSUUIDFromString:(NSString *)UUIDString {
    return [[NSUUID alloc] initWithUUIDString:UUIDString];
}

+(void)main {
  
}
@end
