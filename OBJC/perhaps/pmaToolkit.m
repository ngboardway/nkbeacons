//
//  pmaToolkit.m
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/25/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "pmaToolkit.h"

@implementation pmaToolkit

@synthesize beaconTTL;
-(pmaToolkit*)initForInfo {
    self = [super init];
    if (self) {
        
        beaconTTL = @"10";
        return self;
    }
    return nil;
}

-(NSArray*)allKeysForValueFromDictionary:(NSMutableDictionary *)dictionary forValue:(id)val {
    NSMutableArray *matches = [NSMutableArray array];
    for (id key in dictionary) {
        id value = [dictionary objectForKey:key];
        if(value == val) {
            [matches addObject:key];
        }
    }
    return matches;
}

-(NSArray*)matchesForRegexInText:(NSString *)regex forText:(NSString *)text {
    NSMutableArray* matches = [NSMutableArray array];
    NSRegularExpression *realRegex = [NSRegularExpression regularExpressionWithPattern:(regex) options: NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger length = [text length];
    NSRange range = {0, length};
    NSArray *results = [realRegex matchesInString:text options:0 range:range];
    [results enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
        [matches addObject:obj];
    }];
    return matches;
}

-(id)getObjectFromNotificationForKey:(NSNotification*)notif forKey:(NSString*)key {
    NSMutableDictionary *userInfo = (NSMutableDictionary*)notif.userInfo;
    return [userInfo objectForKey:key];
}
-(void)registerNotication:(id)object forFunction:(NSString*)function withType:(NSString*)type{
    [[NSNotificationCenter defaultCenter] addObserver:object selector:@selector(function) name:type object:nil];
}
-(void)postNotification:(NSString*)type withParameters:(NSMutableDictionary*)parameter {
//    NotificationCenter.default.post(name: Notification.Name(rawValue: type), object: nil, userInfo: parameters)

    [[NSNotificationCenter defaultCenter]  postNotificationName: @"locationChanged:" object:nil userInfo:parameter];
}
@end
