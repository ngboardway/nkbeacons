//
//  AppDelegate.h
//  perhaps
//
//  Created by Natalie Boardway on 4/25/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

