//
//  CoreLocationPlugin.h
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "pmaLocationManager.h"

@interface CoreLocationPlugin : NSObject
+(CLBeaconRegion*) createRegion: (NSUUID*)UUID withIdentifier:(NSString*) identifier;
+(NSUUID*) createNSUUIDFromString: (NSString*)UUIDString;
+(void) main;
@end
