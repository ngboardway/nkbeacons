//
//  pmaToolkit.h
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/25/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface pmaToolkit: NSObject

//@property NSArray* roomAliasReplacements;
@property NSString* beaconTTL;
-(pmaToolkit*)initForInfo;
-(NSArray*)allKeysForValueFromDictionary: (NSMutableDictionary*)dictionary forValue:(id)val;
-(NSArray*)matchesForRegexInText:(NSString*) regex forText:(NSString*)text;
-(id)getObjectFromNotificationForKey:(NSNotification*)notif forKey:(NSString*)key;
-(void)registerNotication:(id)object forFunction:(NSString*)function withType:(NSString*)type;
-(void)postNotification:(NSString*)type withParameters:(NSMutableDictionary*)parameter;
@end
//* pmaToolkit_h */
