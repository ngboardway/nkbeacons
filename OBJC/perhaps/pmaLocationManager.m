//
//  pmaLocationManager.m
//  CoreLocationPlugin
//
//  Created by Natalie Boardway on 4/20/17.
//  Copyright © 2017 Natalie Boardway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "pmaLocationManager.h"
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "pmaCacheManager.h"
#import "pmaLocation.h"
#import "pmaToolkit.h"

static pmaToolkit *toolkit = nil;
static CLLocationManager *locationManager = nil;
static pmaCacheManager *cacheManager = nil;

const char *_ibeaconObjName = "iBeaconPlugin";

@implementation pmaLocationManager
@synthesize sensingType;
@synthesize beacons;
@synthesize locations;
@synthesize beaconsInRange;
@synthesize currentLocation;
@synthesize previousLocation;
@synthesize beaconMap;

-(id)init {
    self = [super init];
    if (self) {
        
        if(! toolkit) {
            toolkit = [[pmaToolkit alloc] init];
        }
        if( !cacheManager) {
            cacheManager = [[pmaCacheManager alloc] init];
        }
        beaconsInRange = [[NSMutableArray alloc] init];
        beaconMap = [[NSMutableDictionary alloc] init];
        beacons = [[NSMutableArray alloc]init];
        locations = [[NSMutableArray alloc] init];
        currentLocation = [[pmaLocation alloc] init];
        previousLocation = [[pmaLocation alloc] init];
        if(!locationManager) {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
        }
        
    }
    return self;
    
}

-(void)loadBeacons:(void (^)(void))handler {
    NSLog(@"%@", @"This is getting called, the beacons have begun");
    id jsonData = [cacheManager loadJSONFileForType:0];
    if (jsonData != nil) {
        id deviceList = jsonData[@"devices"];
        for(id device in deviceList) {
            pmaBeacon *newBeacon = [[pmaBeacon alloc]init];
            newBeacon.alias = device[@"alias"];
            NSString* bMajor = device[@"major"];
            newBeacon.major = [bMajor integerValue];
            NSString* bMinor = device[@"minor"];
            newBeacon.minor = [bMinor integerValue];
            [beacons addObject:newBeacon];
        }
    } else {
        NSLog(@"Didn't find JSON");
    }
    handler();
    void (^testblock)(void) = ^{
        for (pmaBeacon *beacon in beacons) {
            pmaLocation *location = nil;
            if([beacon.alias containsString:@"NK"]) {
                location = [self getNKLocationName:beacon.alias];
                NSLog(@"%@", location.name);
            } else {
                location = [self getLocationFromBeaconAlias:beacon.alias];
            }
            if ([location.beacons count] > 0) {
                [location.beacons addObject:beacon];

            } else {
                location.beacons = [NSMutableArray array];
                [location.beacons addObject:beacon];
            }
//            NSLog(@"%@", beacon);
        }
    };
    testblock();
}


-(void)loadLocations:(void (^)(void))handler {
    enum JSONType type = location;
    id jsonData = [cacheManager loadJSONFileForType:&type];
    if (jsonData != nil) {
        id locationList = jsonData[@"locations"];
        
        for(id location in locationList) {
            pmaLocation *newLocation = [[pmaLocation alloc]init];
            newLocation.name = location[@"Name"];
            newLocation.title = location[@"Title"];
            
            enum floors level = -1;
            NSString *floor = [location[@"Floor"]lowercaseString];
            if([floor isEqualToString:@"ground"])   {
                level = ground;
                } else if ([floor isEqualToString:@"first"]) {
                    level = first;
                } else if ([floor isEqualToString:@"second"]) {
                    level = second;
                }
            newLocation.floor = level;
            
            enum types type = [self getTypesForLocation: newLocation.name withLocationTitle:newLocation.title];
            newLocation.type = type;

            newLocation.enabled = location[@"Open"];
            [locations addObject:newLocation];
            }
    } else {
        NSLog(@"Didn't find JSON");
    }
    handler();

}
-(enum types)getTypesForLocation:(NSString*) locationName withLocationTitle:(NSString*)title {
    [[locationName lowercaseString] containsString:@""];
     if ([[locationName lowercaseString] containsString:@"elevator"]) {
         return elevator;
    } else if ([[locationName lowercaseString] containsString:@"cafe"] || [[title lowercaseString] containsString:@"restaurant"]) {
        return food;
    } else if ([[title lowercaseString] containsString:@"info"]){
        return info;
    } else if ([[locationName lowercaseString] containsString:@"stairs"]|| [[title lowercaseString] containsString:@"stairs"]) {
        return stairs;
    } else if ([[locationName lowercaseString] containsString:@"store"]) {
        return store;
    } else if ([[locationName lowercaseString] containsString:@"bath"]){
        return bathroom;
    } else if ([locationName containsString:@"NK"]) {
        return NK;
    } else {
        return gallery;
    }
}
-(void)startRangingBeacons:(locationSensingType) cursensingType {
    NSLog(@"%@", @"starting to range beacons");
    if ([locations count] ==0 && [beacons count] == 0) {
        [self loadLocations: ^{
         [self loadBeacons: ^{
            self.sensingType = cursensingType;
            [self startRangingBeaconsInRegion];
         }];
        }];
    }
}
-(void)startRangingBeaconsInRegion {
    if ([self areBeaconsLoaded]) {
        if([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse) {
            [locationManager requestWhenInUseAuthorization];
        }
        
        NSUUID *UUID = [[NSUUID alloc] initWithUUIDString:@"f7826da6-4fa2-4e98-8024-bc5b71e0893e"];
        NSString *identifier = @"pmaHackathon";
        CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:UUID identifier:identifier];
        [locationManager startRangingBeaconsInRegion:region];
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if(sensingType == mainBuilding) {
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector: @selector(scanForMainBuildingBeaconsInRange) userInfo:nil repeats:YES];
        }
    }
}
-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
}
-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
}
-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)clbeacons
              inRegion:(CLBeaconRegion*)region {
    if( sensingType == mainBuilding) {
        [self processBeaconsForMainBuilding:clbeacons];
    }
}
-(void)startUpdateHeading {
    
}
-(void)stopUpdateHeading {
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
}
-(void)addBeaconToBeaconsInRange:(CLBeacon*)beacon withProximity:(CLProximity)proximity {
    pmaBeacon *match = [self getPMABeaconForCLBeacon:beacon];
    pmaBeacon *matchCopy = [match copy];
    matchCopy.originalbeacon = beacon;
    matchCopy.lastSeen = [NSDate date];
    
    if(proximity == CLProximityImmediate || proximity == CLProximityNear || proximity == CLProximityFar) {
        
        [beaconsInRange addObject:matchCopy.alias];
        [self initializeBeaconsInRangeWithBeacon:matchCopy];
    
    }
       
    
}
-(NSMutableDictionary*)countOccurenceForBeaconsInRange {
    NSMutableDictionary *countForBeacons = [NSMutableDictionary dictionaryWithCapacity:[beaconsInRange count]];
    for(NSString *currentBeacon in beaconsInRange) {
        float valueForBeacon = [[countForBeacons valueForKey:currentBeacon] floatValue];
        if(valueForBeacon > 0) {
             valueForBeacon = valueForBeacon + 1;
           NSNumber *nsforfloat =  [NSNumber numberWithFloat:valueForBeacon];
            [countForBeacons setValue:nsforfloat forKey:currentBeacon];
        } else {
            NSNumber *nsforint = [NSNumber numberWithInt:1];
            [countForBeacons setValue:nsforint forKey:currentBeacon];
        }
    }
    return countForBeacons;
    
}
-(NSMutableDictionary*)calculateRelativeProbabilityDistributionForBeaconsInRange:(NSMutableDictionary*)listofBeaconsWithOccurenceCount {
    NSMutableDictionary *percentForEachBeacon = [NSMutableDictionary dictionaryWithCapacity: [beaconsInRange count]];
    for(NSString *currentBeacon in [listofBeaconsWithOccurenceCount allKeys]) {
        float count = [[listofBeaconsWithOccurenceCount valueForKey:currentBeacon] floatValue];
        float percent = count/[listofBeaconsWithOccurenceCount count];
        NSNumber *nsforpercent = [NSNumber numberWithFloat:percent];
        [percentForEachBeacon setValue:nsforpercent forKey:currentBeacon];
    }
    return percentForEachBeacon;
}
-(NSMutableDictionary*)calculateProbabilityForLocationsFromBeaconsInRange:(NSMutableDictionary*)probabilityForEachBeacon {
    NSMutableDictionary *locationPercent = [NSMutableDictionary dictionaryWithCapacity:[beaconsInRange count]];
    
    NSNumber *nsinit = [NSNumber numberWithInt:0];
    for(pmaLocation *location in locations) {
        [locationPercent setValue:nsinit forKey:location.name];
    }
    
    for(NSString *beacData in probabilityForEachBeacon) {
        pmaLocation *beacsLocation = nil;
        if([beacData containsString: @"NK"]) {
            beacsLocation = [self getNKLocationName:beacData];
        } else {
            beacsLocation = [self getLocationFromBeaconAlias:beacData];
        }
        float beacprob = [[probabilityForEachBeacon valueForKey:beacData] floatValue];
        float locationInfo = [[locationPercent valueForKey:beacsLocation.name]floatValue];
        locationInfo += beacprob;
        NSNumber *nsforinfo = [NSNumber numberWithFloat:locationInfo];
        [locationPercent setValue:nsforinfo forKey:beacsLocation.name];
    }
    NSMutableDictionary *copy = [locationPercent copy];
    for(NSString *beacLocation in copy) {
        NSNumber *num = [NSNumber numberWithInt:0];
        if([locationPercent valueForKey:beacLocation] == num) {
            [locationPercent removeObjectForKey:beacLocation];
        }
    }
    
    return locationPercent;
}
-(NSMutableDictionary*)assumeCurrentLocation:(NSMutableDictionary*)possibilityForEachLocation {
    NSMutableDictionary* assumedLocation = [NSMutableDictionary dictionary];
    if([self areLocationsLoaded]) {
        NSSortDescriptor *sortIt = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
        NSArray<NSNumber*> *help = [[possibilityForEachLocation allValues]sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortIt]];
               //= [[possibilityForEachLocation allValues] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//            NSInteger value1 = [[NSNumber numberWithInt:(int)obj1]integerValue];
//            NSInteger value2 = [[NSNumber numberWithInt:(int)obj2]integerValue];
//            
//            if(value1 > value2) {
//                return (NSComparisonResult)NSOrderedDescending;
//            }
//            
//            if(value1 < value2) {
//                return (NSComparisonResult)NSOrderedAscending;
//            }
//            return (NSComparisonResult)NSOrderedSame;
//        }];
        
        NSNumber *max = [help objectAtIndex:0];
        pmaLocation  *maybePlace = [[toolkit allKeysForValueFromDictionary:possibilityForEachLocation forValue:max]firstObject];
        [assumedLocation setObject:max forKey:maybePlace];
    }
    return assumedLocation;
}
-(void)processBeaconsForMainBuilding:(NSArray*)clBeacons {
    NSMutableArray *knownBeaconsList = [NSMutableArray array];
    [clBeacons enumerateObjectsUsingBlock:^(CLBeacon * obj, NSUInteger idx, BOOL * stop) {
        if(obj.proximity != CLProximityUnknown) {
            [knownBeaconsList addObject:obj];
        }
    }];
    [knownBeaconsList sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        CLBeacon *beacon1 = (CLBeacon*)obj1;
        CLBeacon *beacon2 = (CLBeacon*)obj2;
        
        if(beacon1.proximity > beacon2.proximity) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if(beacon1.proximity < beacon2.proximity) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    for(CLBeacon *beacon in knownBeaconsList) {
        pmaBeacon *foundyou = [self getPMABeaconForCLBeacon:beacon];
        if(foundyou != nil) {
        [self addBeaconToBeaconsInRange:beacon withProximity:beacon.proximity];
        }
    }
    
}
-(void)scanForMainBuildingBeaconsInRange {
    if([self areBeaconsLoaded]) {
        NSInteger TTL = [[NSNumber numberWithInt:10] integerValue];
        NSArray *beacs = [[beaconsInRange reverseObjectEnumerator] allObjects];
        for(int i = 0; i < [beaconsInRange count]; i--) {
            pmaBeacon *beacondata = [beaconMap objectForKey:[beacs objectAtIndex:i]];
            NSInteger timeSinceLastSeen = [[NSCalendar currentCalendar] components:NSCalendarUnitSecond fromDate:beacondata.lastSeen toDate:[NSDate date] options:0].second;
            if(timeSinceLastSeen >= TTL) {
                [beaconsInRange removeObjectAtIndex:i];
            }
        }
        NSMutableDictionary *countForBeaconsInRange = [self countOccurenceForBeaconsInRange];
        NSLog(@"This many beacons in range %lu", (unsigned long)[countForBeaconsInRange count]);
        NSMutableDictionary *probabiltyForEachBeacon = [self calculateRelativeProbabilityDistributionForBeaconsInRange:countForBeaconsInRange];
//        NSLog(@"probability for them %lu", (unsigned long)[probabiltyForEachBeacon count]);
        NSMutableDictionary *probabilityForEachLocation = [self calculateProbabilityForLocationsFromBeaconsInRange:probabiltyForEachBeacon];
//        NSLog(@"probability for locations %@", );
        
        if([probabilityForEachLocation count] > 0) {
            NSMutableDictionary *probs = [self assumeCurrentLocation:probabilityForEachLocation];
            pmaLocation *location = [self getLocationForName:[[probs allKeys] firstObject]];
         [self updateCurrentLocation:location];
       }
    }
}
    
-(void)updateCurrentLocation:(pmaLocation*)location {
    if([currentLocation.name isEqualToString:location.name]) {
        
    } else {
        previousLocation = currentLocation;
        currentLocation = location;
        [toolkit postNotification:@"locationChanged" withParameters:[NSMutableDictionary dictionaryWithObject:location forKey:@"currentLocation"]];
    }
}
-(pmaBeacon*)getPMABeaconForCLBeacon:(CLBeacon*)originalBeacon {
    for(pmaBeacon *pBeacon in beacons) {
        if(pBeacon.major == [originalBeacon.major integerValue] && pBeacon.minor == [originalBeacon.minor integerValue]) {
            return pBeacon;
        }
    }
    return nil;
}
-(pmaLocation*)getLocationForBeacon:(pmaBeacon*)beacon {
    for(pmaLocation *location in locations) {
        if ([location respondsToBeacon:beacon]) {
            return location;
        }
    }
    return nil;
}
-(pmaLocation*)getCurrentLocation {
    return currentLocation;
}
-(BOOL)areBeaconsLoaded {
   return [beacons count] > 0;
 }

-(BOOL)areLocationsLoaded {
    return [locations count] > 0;
}

-(pmaLocation*)getLocationFromBeaconAlias:(NSString*)alias {
    NSArray *roomAliasReplacements = [NSArray arrayWithObjects: @"_L", @"_R", @"_C", @"_T", @"_M", @"_B", nil];
    NSArray *matches = [toolkit matchesForRegexInText:(@"[_][A-Z]{1}") forText:alias];
    
    if([matches count] > 0 ) {
        NSString *result = alias;
        for (NSString *r in roomAliasReplacements) {
            result = [result stringByReplacingOccurrencesOfString:r withString:@""];
        }
        NSLog(@"%@", result);
        return [self getLocationForName:result];
    } else {
        pmaLocation *boohiss = [self getLocationForName:alias];
        return boohiss;
    }
    return nil;
}

-(pmaLocation*)getLocationForName:(NSString*)name {
    for(pmaLocation *location in locations) {
        if ([location.name isEqualToString: name]) {
            return location;
        }
    }
    return nil;
}

-(pmaLocation*)getNKLocationName:(NSString*)name {
    NSString *locationName = @"";
    if([name  isEqual: @"NK001-k"]) {
        locationName = @"back office";
    } else if ([name  isEqual: @"NK002-k"]) {
        locationName = @"front office";
    } else if ([name  isEqual: @"NK003-k"]) {
        locationName = @"library";
    } else if ([name  isEqual: @"NK004-k"]) {
        locationName = @"living room";
    } else if ([name  isEqual: @"NK005-k"]) {
        locationName = @"meeting room";
    }
    return [self getLocationForName:locationName];
}
-(pmaBeacon*)getBeaconFromAlias:(NSString*)alias {
    for(pmaBeacon *beacon in beacons) {
        if([beacon.alias isEqualToString:alias]) {
            return beacon;
        }
    }
    return nil;
}
-(void)initializeBeaconsInRangeWithBeacon:(pmaBeacon*)addedBeacon {
    for(NSString *beaconAlias in beaconsInRange) {
        if ([beaconMap objectForKey:beaconAlias] != nil) {
            [beaconMap setObject:addedBeacon forKey:beaconAlias];
        } else {
            [beaconMap setObject:addedBeacon forKey:beaconAlias];
        }
    }
}
@end
